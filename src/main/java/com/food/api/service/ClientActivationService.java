package com.food.api.service;

import com.food.api.model.Client;
import com.food.api.notification.Notify;
import org.springframework.stereotype.Component;

@Component
public class ClientActivationService{

  private Notify notifier;

  public ClientActivationService(Notify notifier){
    this.notifier=notifier;
  }

  public String active(Client client){
    client.active();

    return notifier.notify(client, "Seu cadastro no sistema está ativo!");
  }
}