package com.food.api.controllers;

import com.food.api.model.Client;
import com.food.api.service.ClientActivationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class FirstController{

  @Autowired
  private ClientActivationService service;

  @GetMapping
  public String hello(){
    var cliente=new Client("Junior", "junior@gmail.com", "12345678");
    return service.active(cliente);
  }
}
