package com.food.api.notification;

import com.food.api.model.Client;

public interface Notify{
  String notify(Client client, String message);
}
