package com.food.api.notification;

import com.food.api.model.Client;
import org.springframework.stereotype.Component;

@Component
public class EmailNotifier implements Notify{
  public String notify(Client client, String message){
    return "Notificando "+client.getName()+" através dp e-mail: "+client.getEmail()+": "+message;
  }
}
